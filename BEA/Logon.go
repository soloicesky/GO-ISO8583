package BEA

import (
)

/**
	根据提供的现有交易数据执行签到
	@param transData 交易数据
	@param config 后台配置参数
	如有出错则返回错误信息 err不为nil，否则err为nil 交易数据为当前交易数据
**/
func Logon(transData TransactionData, config Config) (TransactionData, error) {
	var fields []byte = []byte{0, 3,  11,  24, 41,}

	transData.TransType = LOGON
	var err error
	transData, err = CreatAndSendAutoriztion(transData, config, fields)

	return transData, err
}
