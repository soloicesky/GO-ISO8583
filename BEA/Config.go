package BEA

import (
	_"fmt"
	_"gopkg.in/yaml.v2"
	_"io/ioutil"
)

// type Config struct{
// 	Host string `yaml:"Host"`
// 	MerchantId string `yaml:"MerchantId"`
// 	TerminalId string `yaml:"TerminalId"`
// 	TPDU string `yaml:"TPDU"`
// 	EDS string `yaml:"EDS"`
// }
//后台配置参数
type Config struct {
	Host       string //后台地址 
	MerchantId string //商户号
	TerminalId string //终端号
	TPDU       string //tpdu
	EDS        string //eds
}

// var config Config

// func Init() {
// 	data, err := ioutil.ReadFile("../Config.yaml")
// 	if err != nil {
// 		panic(err)
// 	}

// 	err = yaml.Unmarshal([]byte(data), &config)

// 	if err != nil {
// 		panic(err)
// 	}

// 	fmt.Print(config)
// }

// func GetHost() string {
// 	return config.Host
// }

// func GetMerchantId() string {
// 	return config.MerchantId
// }

// func GetTerminalId() string {
// 	return config.TerminalId
// }

// func GetTpdu() string {
// 	return config.TPDU
// }

// func GetEds() string {
// 	return config.EDS
// }
