package BEA

type TransactionData struct {
	Stan           string            //流水号
	Invoice        string            //发票号
	Date           string            //交易日期
	Time           string            //交易时间
	Amount         string            //授权金额
	TransType      string            //交易类型
	CurrencyCode   string            //货币代码
	Pinblock       string            //联机PINBLOCK
	MerchantId     string            //商户号
	TerminalId     string            //终端号
	Pan            string            //主账号
	PanSeqNo       string            //卡片序列号
	CardExpireDate string            //有效期
	Track1         string            //磁道一
	Track2         string            //磁道二
	PosEntryMode   string            //刷卡方式
	IccRelatedData map[string]string //IC卡相关数据
	Rrn            string            //系统参考号
	AuthCode       string            //授权码
	ResponseCode   string            //响应码
	OriginalRrn    string            //原交易参考号
	OriginalAmount string            //原交易金额
	OriginalTransType string		 //原交易类型
}

type  ReconciliationTotals struct{
	CapturedSalesCount string
	CapturedSalesAmount string
	RefundCount string
	RefundAmount string
	
}
