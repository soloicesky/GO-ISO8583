package BEA

import (
)

/**
	撤销退货
	transData 交易数据
	config 后台配置参数
**/
func VoidRefund(transData TransactionData, config Config) (TransactionData, error) {
	var fields []byte


	switch transData.PosEntryMode {
	case INSERT:
		fields = []byte{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 55, 62}
	case SWIPE:
		fields = []byte{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 62}
	case WAVE:
		fields = []byte{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 55, 62}
	case FALLBACK:
		fields = []byte{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 62}
	case MSD:
		fields = []byte{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 62}
	case MANUAL:
		fields = []byte{0, 2, 3, 4, 11, 14, 22, 24, 25, 37, 41, 42, 62}
	}

	var err error
	transData, err = CreatAndSendAutoriztion(transData, config, fields)

	return transData, err
}
